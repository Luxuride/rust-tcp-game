# Tcp guess game
## How to run
If you dont have cargo installed run or follow [https://www.rust-lang.org/tools/install](https://www.rust-lang.org/tools/install)
```
./install-rust.sh
```

All of the following scripts are wrappers for cargo run:

You can run server with
```
./run-server.sh
```

You can client with
```
./run-client-tcp.sh
```
or
```
./run-client-socket.sh
```

## Packets
### Login
#### Client
[action][password]
#### Server
Closed connection or

[request-status][id]
### Client -> Server
The packets are mostly

[action][id][string]

Action is required and the rest is required only in some actions

### Server -> Client
[request-status][action-status][id][string]

Request-status is required, the rest is required only in some actions