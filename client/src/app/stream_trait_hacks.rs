use std::{io, net::TcpStream, os::unix::net::UnixStream};

pub trait StreamTraitHacks<T> {
    fn set_nonblocking_hack(&mut self, value: bool) -> io::Result<()>;
    fn try_clone_hack(&mut self) -> io::Result<T>;
}

impl StreamTraitHacks<TcpStream> for TcpStream {
    fn set_nonblocking_hack(&mut self, value: bool) -> io::Result<()> {
        self.set_nonblocking(value)
    }
    
    fn try_clone_hack(&mut self) -> io::Result<TcpStream> {
        return self.try_clone()
    }
}

impl StreamTraitHacks<UnixStream> for UnixStream {
    fn set_nonblocking_hack(&mut self, value: bool) -> io::Result<()> {
        self.set_nonblocking(value)
    }
    
    fn try_clone_hack(&mut self) -> io::Result<UnixStream> {
        return self.try_clone()
    }
}