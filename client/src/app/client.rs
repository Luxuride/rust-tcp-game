use std::{error::Error, io::{self, BufRead, BufReader, Read, Write}, net::TcpStream, os::unix::net::UnixStream};

use super::{actions::Actions, stream_trait_hacks::StreamTraitHacks};

pub struct Client<T> {
    stream: T,
    id: Option<u64>,
}

impl Client<TcpStream>  {
    pub fn new(address: &str) -> Result<Client<TcpStream>, Box<dyn Error>> {
        let stream = TcpStream::connect(address)?;
        Ok(Client::<TcpStream> {
            stream,
            id: None,
        })
    }
}

impl Client<UnixStream> {
    pub fn new(address: &str) -> Result<Client<UnixStream>, Box<dyn Error>> {
        let stream = UnixStream::connect(address)?;
        Ok(Client::<UnixStream> {
            stream,
            id: None,
        })
    }
}

impl<T> Client<T>
where
T: Write + Read + Send + 'static + StreamTraitHacks<T> {

    pub fn run(&mut self) -> Result<(), Box<dyn Error>> {
        self.sign_in()?;
        loop {
            print!("guess | host | players: ");
            io::stdout().flush()?;
            let mut buffer = String::new();
            io::stdin().read_line(&mut buffer)?;
            match buffer.trim() {
                "guess" => return self.play_guess(),
                "host" => return self.play_host(),
                "players" => self.read_opponents()?,
                _ => println!("Try again"),
            }
        }
    }

    fn sign_in(&mut self) -> Result<(), Box<dyn Error>> {
        let password = Self::read_u64("Password: ")?.to_ne_bytes();
        self.stream.write(&u8::from(Actions::SignIn).to_ne_bytes())?;
        self.stream.write(&password)?;
        let mut status = [0; 1];
        self.stream.read(&mut status)?;
        let mut id = [0; 8];
        self.stream.read(&mut id)?;
        self.id = Some(u64::from_ne_bytes(id));
        Ok(())
    }

    fn read_opponents(&mut self) -> Result<(), Box<dyn Error>> {
        let mut res = vec![];
        res.append(&mut u8::from(Actions::RequestOponents).to_be_bytes().to_vec());
        self.stream.write(&res)?;
        let mut status = [0; 1];
        self.stream.read(&mut status)?;
        if u8::from_ne_bytes(status) == 0 {
            println!("Error");
        } else {
            println!("Success");
        }
        let mut ids = [0; 8];
        // No Delay
        self.stream.set_nonblocking_hack(true)?;
        while !self.stream.read(&mut ids).is_err() {
            print!("{} ", u64::from_ne_bytes(ids));
        }
        self.stream.set_nonblocking_hack(false)?;
        println!();
        Ok(())
    }

    fn read_u64(message: &str) -> Result<u64, Box<dyn Error>> {
        print!("{}", message);
        io::stdout().flush()?;
        let mut buffer = String::new();
        io::stdin().read_line(&mut buffer)?;
        match buffer.trim().parse::<u64>() {
            Ok(res) => Ok(res),
            Err(_) => {
                println!("\ninvalid number");
                Self::read_u64(message)
            }
        }
    }

    fn play_host(&mut self) -> Result<(), Box<dyn Error>> {
        loop {
            let mut res = vec![];
            res.append(&mut u8::from(Actions::RequestMatch).to_ne_bytes().to_vec());
            res.append(&mut Self::read_u64("User ID: ")?.to_ne_bytes().to_vec());
            print!("secret: ");
            io::stdout().flush()?;
            let mut secret = String::new();
            io::stdin().read_line(&mut secret)?;
            res.append(&mut secret.as_bytes().to_vec());
            self.stream.write(&mut res)?;
            let mut status = [0; 1];
            self.stream.read(&mut status)?;
            let status = u8::from_ne_bytes(status);
            match status {
                0 => eprintln!("Invalid id"),
                1 => {
                    println!("Success");
                    break;
                }
                _ => eprintln!("Invalid server response"),
            }
        }
        let mut write_stream = self.stream.try_clone_hack()?;
        std::thread::spawn(move || {
            loop {
                print!("Write hint: ");
                io::stdout().flush().unwrap();
                let mut hint = String::new();
                // Would use tokio for async stdin
                io::stdin().read_line(&mut hint).unwrap();
                write_stream.write(&mut u8::from(Actions::HintMatch).to_ne_bytes().to_vec()).unwrap();
                write_stream.write(hint.as_bytes()).unwrap();
            }
        });
        let mut buf = BufReader::new(&mut self.stream);
        loop {
            let mut status = [0; 1];
            if buf.read(&mut status)? == 0 {
                // Handle server disconnect
                return Ok(());
            };
            let status = u8::from_ne_bytes(status);
            if status == 0 {
                eprintln!("Internal server error");
                continue;
            }
            let mut result = [0; 1];
            buf.read(&mut result)?;
            let result = u8::from_ne_bytes(result);
            if result == 1 {
                println!("\nPlayer won");
                break;
            }
            if result == 2 {
                println!("\nPlayer lost");
                break;
            }
            if result > 2 {
                eprintln!("Internal server error");
                continue;
            }
            let mut guess = String::new();
            buf.read_line(&mut guess)?;
            print!("\nPlayer guessed: {}", guess);
            print!("Write hint: ");
            io::stdout().flush()?;
        }

        Ok(())
    }

    fn play_guess(&mut self) -> Result<(), Box<dyn Error>> {
        let mut res = vec![];
        res.append(&mut u8::from(Actions::RequestAssignement).to_ne_bytes().to_vec());
        self.stream.write(&mut res)?;
        println!("Waiting!...");
        loop {
            let mut state = [0; 1];
            self.stream.read(&mut state)?;
            let state = u8::from_ne_bytes(state);
            if state == 1 {
                break;
            };
        }
        println!("Game Start");
        let mut write_stream = self.stream.try_clone_hack()?;
        std::thread::spawn(move || {
            loop {
                print!("Write guess: ");
                io::stdout().flush().unwrap();
                let mut guess = String::new();
                // Would use tokio for async stdin
                io::stdin().read_line(&mut guess).unwrap();
                write_stream.write(&mut u8::from(Actions::GuessMatch).to_ne_bytes().to_vec()).unwrap();
                write_stream.write(guess.as_bytes()).unwrap();
            }
        });
        loop {
            let mut state = [0; 1];
            if self.stream.read(&mut state)? == 0 {
                // Handle server disconnect
                return Ok(());
            };
            let state = u8::from_ne_bytes(state);
            if state == 0 {
                eprintln!("Error")
            };
            let mut res_type = [0; 1];
            self.stream.read(&mut res_type)?;
            match u8::from_ne_bytes(res_type) {
                0 => {
                    let mut hint: String = String::new();
                    let mut buf = BufReader::new(&mut self.stream);
                    buf.read_line(&mut hint)?;
                    print!("\nHint: {}", hint);
                    print!("Write guess: ");
                    io::stdout().flush()?;
                }
                1 => {
                    println!("\nVictory");
                    return Ok(());
                }
                2 => {
                    print!("\nLoss");
                    return Ok(());
                }
                _ => panic!(),
            }
        }
    }
}
