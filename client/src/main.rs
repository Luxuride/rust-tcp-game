use std::{
    env, error::Error, net::TcpStream, os::unix::net::UnixStream
};

use app::client::Client;

mod app;

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();
    let protocol = String::from("tcp");
    let address = String::from("127.0.0.1:7878");
    let protocol: &String = args.get(1).unwrap_or(&protocol);
    let address: &String = args.get(2).unwrap_or(&address);
    if protocol == "tcp" {
        let mut client = Client::<TcpStream>::new(&address)?;
        client.run()?;
    } else {
        let mut client = Client::<UnixStream>::new(address)?;
        client.run()?;
    }
    Ok(())
}