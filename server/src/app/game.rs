use std::sync::mpsc::Sender;

#[derive(PartialEq, Eq, Clone, Debug)]
pub enum State {
    Started,
    WaitingForGame,
    HostingGame(u64, String),
    PlayingGame(u64),
}

pub enum Request {
    Start,
    Hint(String),
    Victory,
    Loss,
}

pub struct Game {
    pub state: State,
    pub sender: Sender<Request>,
}
