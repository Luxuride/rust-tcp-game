use std::{error::Error, fmt::Display};

#[derive(PartialEq, Eq)]
pub enum Actions {
    SignIn,
    RequestOponents,
    RequestAssignement,
    RequestMatch,
    HintMatch,
    GuessMatch,
    GiveUp,
}
impl TryFrom<u8> for Actions {
    type Error = ActionError;
    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(Self::SignIn),
            1 => Ok(Self::RequestOponents),
            2 => Ok(Self::RequestAssignement),
            3 => Ok(Self::RequestMatch),
            4 => Ok(Self::HintMatch),
            5 => Ok(Self::GuessMatch),
            6 => Ok(Self::GiveUp),
            _ => Err(ActionError {}),
        }
    }
}
impl From<Actions> for u8 {
    fn from(value: Actions) -> Self {
        match value {
            Actions::SignIn => 0,
            Actions::RequestOponents => 1,
            Actions::RequestAssignement => 2,
            Actions::RequestMatch => 3,
            Actions::HintMatch => 4,
            Actions::GuessMatch => 5,
            Actions::GiveUp => 6,
        }
    }
}

#[derive(Debug)]
pub struct ActionError {}
impl Display for ActionError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Invalid Action")
    }
}
impl Error for ActionError {}
