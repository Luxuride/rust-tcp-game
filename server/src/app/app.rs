use std::{
    collections::HashMap,
    error::Error,
    fmt::Display,
    io::{BufRead, BufReader, Read, Write},
    sync::{
        mpsc::{self, Receiver},
        Arc, Mutex,
    },
};

use super::{
    actions::Actions,
    game::{Game, Request, State},
    stream_trait_hacks::StreamTraitHacks,
};

#[derive(Debug)]
pub struct LoginError {
    message: String,
}

impl LoginError {
    fn new(message: &str) -> Self {
        Self {
            message: message.to_owned(),
        }
    }
}

impl Display for LoginError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl Error for LoginError {}

pub struct App<T> {
    games: Arc<Mutex<HashMap<u64, Game>>>,
    stream: T,
    id: Option<u64>,
    receiver: Option<Receiver<Request>>,
    password: u64,
}

impl<T> App<T>
where
    T: Read + Write + Send + 'static + StreamTraitHacks<T>,
{
    pub fn new(stream: T, games: Arc<Mutex<HashMap<u64, Game>>>, password: u64) -> App<T> {
        return App {
            stream,
            games,
            id: None,
            receiver: None,
            password,
        };
    }
    pub fn run(mut self) -> Result<(), Box<dyn Error>> {
        loop {
            self.handle_connection()?;
        }
    }

    fn invalid_request(&mut self) -> Result<(), Box<dyn Error>> {
        self.stream.write(&0_u8.to_ne_bytes())?;
        Ok(())
    }

    // Accepts [action][password]
    fn sign_in(&mut self) -> Result<(), Box<dyn Error>> {
        let mut password = [0; 8];
        self.stream.read(&mut password)?;
        let password = u64::from_ne_bytes(password);
        if password != self.password {
            return Err(Box::new(LoginError::new("Invalid Password")));
        }
        let mut games = self.games.lock().unwrap();
        loop {
            // Generate random ID
            let id = rand::random::<u64>();
            if games.contains_key(&id) {
                continue;
            }
            let (sender, receiver) = mpsc::channel::<Request>();
            self.receiver = Some(receiver);
            let game = Game {
                state: State::Started,
                sender,
            };
            self.id = Some(id);
            games.insert(id, game);
            break;
        }
        let id = self.id.unwrap();
        let mut res = vec![];
        res.append(&mut 1_u8.to_ne_bytes().to_vec());
        res.append(&mut id.to_ne_bytes().to_vec());
        self.stream.write(&res)?;
        Ok(())
    }

    // Accepts [action]
    // Returns list of ids
    // The list consists of players waiting tu be guessers
    fn request_oponents(&mut self) -> Result<(), Box<dyn Error>> {
        let mut players = vec![];
        for (id, game) in self.games.lock().unwrap().iter() {
            if game.state == State::WaitingForGame {
                let player = id.to_ne_bytes();
                players.append(&mut Vec::from(player));
            }
        }
        let mut res = vec![];
        res.append(&mut 1_u8.to_ne_bytes().to_vec());
        res.append(&mut players);
        // Response
        self.stream.write(&res)?;
        Ok(())
    }

    // Accepts [action]
    // Player requests to become guesser
    fn request_assignement(&mut self) -> Result<(), Box<dyn Error>> {
        self.games
            .lock()
            .unwrap()
            .get_mut(&self.id.unwrap())
            .unwrap()
            .state = State::WaitingForGame;
        let _client = self.receiver.as_ref().unwrap().recv()?;
        // Response
        self.stream.write(&mut 1_u8.to_ne_bytes())?;
        self.play_guest()
    }

    // Accepts [action][text]
    // Handles guess text
    fn play_guest(&mut self) -> Result<(), Box<dyn Error>> {
        loop {
            let mut action = [0; 1];
            self.stream.set_nonblocking_hack(true)?;
            if !self.stream.read(&mut action).is_err() {
                self.stream.set_nonblocking_hack(false)?;
                let action = Actions::try_from(u8::from_ne_bytes(action))?;
                if Actions::GuessMatch != action {
                    self.invalid_request()?;
                    continue;
                }
                self.guess_match()?;
            }
            if let Ok(request) = self.receiver.as_mut().unwrap().try_recv() {
                match request {
                    Request::Hint(hint) => {
                        let mut res = vec![];
                        res.append(&mut 1_u8.to_ne_bytes().to_vec());
                        res.append(&mut 0_u8.to_ne_bytes().to_vec());
                        res.append(&mut hint.as_bytes().to_vec());
                        self.stream.write(&mut res)?;
                    }
                    Request::Victory => {
                        let mut res = vec![];
                        res.append(&mut 1_u8.to_ne_bytes().to_vec());
                        res.append(&mut 1_u8.to_ne_bytes().to_vec());
                        self.stream.write(&mut res)?;
                        return Ok(());
                    }
                    Request::Loss => {
                        let mut res = vec![];
                        res.append(&mut 1_u8.to_ne_bytes().to_vec());
                        res.append(&mut 2_u8.to_ne_bytes().to_vec());
                        self.stream.write(&mut res)?;
                        return Ok(());
                    }
                    _ => (),
                }
            }
        }
    }

    // Accepts [action][id][text]
    // Player requests to be host for requested guesser
    fn request_match(&mut self) -> Result<(), Box<dyn Error>> {
        {
            let mut id = [0; 8];
            self.stream.read(&mut id)?;
            let id = u64::from_ne_bytes(id);

            let games = self.games.clone();
            let mut games = games.lock().unwrap();
            match games.get(&id) {
                Some(oponent) => {
                    if oponent.state != State::WaitingForGame {
                        BufReader::new(&mut self.stream).read_line(&mut String::new())?;
                        return self.invalid_request();
                    }
                    oponent.sender.send(Request::Start)?;
                    self.stream.write(&1_u8.to_ne_bytes())?;
                }
                // Oponent does not exist
                None => {
                    // Clean stream
                    BufReader::new(&mut self.stream).read_line(&mut String::new())?;
                    return self.invalid_request();
                }
            };

            let mut buf_reader = BufReader::new(&mut self.stream);
            games.get_mut(&id).unwrap().state = State::PlayingGame(self.id.unwrap());
            let mut secret = String::new();
            buf_reader.read_line(&mut secret)?;
            games.get_mut(&self.id.unwrap()).unwrap().state = State::HostingGame(id, secret);
        }
        self.host_match()
    }

    // Accepts [action][string]
    // Handles the host player match 
    fn host_match(&mut self) -> Result<(), Box<dyn Error>> {
        loop {
            let mut action = [0; 1];
            self.stream.set_nonblocking_hack(true)?;
            if !self.stream.read(&mut action).is_err() {
                self.stream.set_nonblocking_hack(false)?;
                let action = Actions::try_from(u8::from_ne_bytes(action))?;
                if Actions::HintMatch != action {
                    self.invalid_request()?;
                    continue;
                }
                let _ = self.hint_match();
            }
            if let Ok(request) = self.receiver.as_ref().unwrap().try_recv() {
                match request {
                    Request::Start => self.invalid_request()?,
                    Request::Hint(guess) => {
                        self.stream.write(&1_u8.to_ne_bytes())?;
                        self.stream.write(&0_u8.to_ne_bytes())?;
                        self.stream.write(&mut guess.as_bytes())?;
                    }
                    Request::Victory => {
                        self.stream.write(&1_u8.to_ne_bytes())?;
                        self.stream.write(&1_u8.to_ne_bytes())?;
                        return Ok(());
                    }
                    Request::Loss => {
                        self.stream.write(&1_u8.to_ne_bytes())?;
                        self.stream.write(&2_u8.to_ne_bytes())?;
                        return Ok(());
                    }
                }
            }
        }
    }

    // Handles play_match [string]
    // Handles text part of hint
    fn hint_match(&mut self) -> Result<(), Box<dyn Error>> {
        let mut buf_reader = BufReader::new(&mut self.stream);
        let mut hint = String::new();
        buf_reader.read_line(&mut hint)?;
        let games = self.games.clone();
        let games = games.lock().unwrap();
        println!("{:?}", games.get(&self.id.unwrap()).unwrap().state.clone());
        // Get oponent ID
        let State::HostingGame(oponent_id, _) = games.get(&self.id.unwrap()).unwrap().state else {
            return self.invalid_request();
        };
        let oponent = games.get(&oponent_id).unwrap();
        // Response
        oponent.sender.send(Request::Hint(hint))?;
        Ok(())
    }

    // Handles play_guest [text]
    // Handles text part of guess
    fn guess_match(&mut self) -> Result<(), Box<dyn Error>> {
        let mut buf_reader = BufReader::new(&mut self.stream);
        let mut guess = String::new();
        buf_reader.read_line(&mut guess)?;
        let games = self.games.clone();
        let games = games.lock().unwrap();
        let player = games.get(&self.id.unwrap()).unwrap();
        let State::PlayingGame(host_id) = player.state else {
            return self.invalid_request();
        };
        let host = games.get(&host_id).unwrap();
        let State::HostingGame(_, res) = host.state.clone() else {
            panic!()
        };
        host.sender.send(Request::Hint(guess.clone()))?;
        if res == guess {
            player.sender.send(Request::Victory)?;
            host.sender.send(Request::Victory)?;
        }
        Ok(())
    }

    // Accepts [action]
    // Returns [status][event] to guest + host
    fn give_up(&mut self) -> Result<(), Box<dyn Error>> {
        let games = self.games.clone();
        let games = games.lock().unwrap();
        let player = games.get(&self.id.unwrap()).unwrap();
        player.sender.send(Request::Loss)?;
        Ok(())
    }


    fn handle_connection(&mut self) -> Result<(), Box<dyn Error>> {
        let mut action = [0; 1];
        self.stream.read(&mut action)?;
        let action = Actions::try_from(u8::from_ne_bytes(action))?;

        // Crash only if sign in errors, otherwise send invalid request to client
        if let Err(error) = match action {
            // Some of these Actions got deprecated and unreachable due to different approach to implementation in some cases.
            // This would be first thing to fix in refactor
            Actions::SignIn => Ok(self.sign_in()?),
            Actions::RequestOponents => self.request_oponents(),
            Actions::RequestAssignement => self.request_assignement(),
            Actions::RequestMatch => self.request_match(),
            Actions::HintMatch => self.hint_match(),
            Actions::GuessMatch => self.guess_match(),
            Actions::GiveUp => self.give_up(),
        } {
            eprintln!("Error: {}", error);
            self.invalid_request()?;
        }
        Ok(())
    }
}
