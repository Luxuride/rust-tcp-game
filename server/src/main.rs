use std::{
    collections::HashMap, env, error::Error, net::TcpListener, os::unix::net::UnixListener, path::Path, sync::{Arc, Mutex}
};

use app::game::Game;

use crate::app::app::App;

mod app;

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();
    let password = args.get(1).expect("Password is required").parse::<u64>().expect("Pasword must be u64");
    let tcp_address = String::from("127.0.0.1:7878");
    let unix_address = String::from("/tmp/my_app.sock");
    let tcp_address: &String = args.get(2).unwrap_or(&tcp_address);
    let unix_address: &String = args.get(3).unwrap_or(&unix_address);
    let games = Arc::new(Mutex::new(HashMap::new()));
    let tcp_server = Server::<TcpListener>::new(&tcp_address, games.clone(), password)?;
    let unix_server = Server::<UnixListener>::new(&unix_address, games.clone(), password)?;
    std::thread::spawn(move || {
        tcp_server.listen().unwrap();
    });
    unix_server.listen()?;
    Ok(())
}

struct Server<T> {
    listener: Box<T>,
    games: Arc<Mutex<HashMap<u64, Game>>>,
    password: u64,
}

trait ListenerTraitHack {
    fn bind_hack(address: &str) -> Result<Box<Self>, std::io::Error>;
}

impl ListenerTraitHack for TcpListener {
    fn bind_hack(address: &str) -> Result<Box<Self>, std::io::Error> {
        return Ok(Box::new(TcpListener::bind(address)?))
    }
}

impl ListenerTraitHack for UnixListener {
    fn bind_hack(address: &str) -> Result<Box<Self>, std::io::Error> {
        let socket = Path::new(address);
        if socket.exists() {
            std::fs::remove_file(socket)?;
        }
        return Ok(Box::new(UnixListener::bind(socket)?))
    }
}

impl<T> Server<T>
where
T: ListenerTraitHack {
    pub fn new(address: &str, games: Arc<Mutex<HashMap<u64, Game>>>, password: u64) -> Result<Server<T>, Box<dyn Error>> {
        let listener = T::bind_hack(address)?;
        Ok(Server { listener, games, password })
    }
}

impl Server<TcpListener> {
    pub fn listen(self) -> Result<(), Box<dyn Error>> {
        for stream in self.listener.incoming() {
            let stream = stream.unwrap();
            println!("Connection established!");
            let app = App::new(stream, self.games.clone(), self.password);
            std::thread::spawn(move || {
                app.run().unwrap();
            });
        }
        Ok(())
    }
}

impl Server<UnixListener> {
    pub fn listen(self) -> Result<(), Box<dyn Error>> {
        for stream in self.listener.incoming() {
            let stream = stream.unwrap();
            println!("Connection established!");
            let app = App::new(stream, self.games.clone(), self.password);
            std::thread::spawn(move || {
                app.run().unwrap();
            });
        }
        Ok(())
    }
}