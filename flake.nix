{
  inputs = { nixpkgs.url = "github:nixos/nixpkgs"; };

  outputs = { self, nixpkgs }:
    let
    pkgs = nixpkgs.legacyPackages.x86_64-linux;
    rust-toolchain = pkgs.symlinkJoin {
      name = "rust-toolchain";
      paths = [pkgs.rustc pkgs.cargo pkgs.rustPlatform.rustcSrc pkgs.rustfmt pkgs.clippy];
    };
    in {
      devShell.x86_64-linux =
        pkgs.mkShell {
          buildInputs = with pkgs; [
            rust-toolchain
            openssl
          ];

          RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
        };
    };
}
